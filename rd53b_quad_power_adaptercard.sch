EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RD53B Quad Power Adapter Card"
Date "2020-12-23"
Rev "1.0"
Comp "LBNL"
Comment1 "Aleksandra Dimitrievska"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FTM-108-02-L-DV:FTM-108-02-L-DV J1
U 1 1 600C66C4
P 1850 1650
F 0 "J1" H 1850 2217 50  0000 C CNN
F 1 "FTM-108-02-L-DV" H 1850 2126 50  0000 C CNN
F 2 "Library:SAMTEC_FTM-108-02-L-DV" H 1850 1650 50  0001 L BNN
F 3 "E" H 1850 1650 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 1850 1650 50  0001 L BNN "Field4"
F 5 "SAMTEC" H 1850 1650 50  0001 L BNN "Field5"
	1    1850 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector:LEMO2 J6
U 1 1 600D2696
P 4500 1000
F 0 "J6" H 4680 1096 50  0000 L CNN
F 1 "LEMO2" H 4680 1005 50  0000 L CNN
F 2 "Library:lemo_epb00250ntn" H 4500 1050 50  0001 C CNN
F 3 " ~" H 4500 1050 50  0001 C CNN
	1    4500 1000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x01 J3
U 1 1 600D4038
P 3650 3500
F 0 "J3" H 3700 3717 50  0000 C CNN
F 1 "Conn_02x01" H 3700 3626 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3650 3500 50  0001 C CNN
F 3 "~" H 3650 3500 50  0001 C CNN
	1    3650 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J8
U 1 1 600D4842
P 3700 3000
F 0 "J8" H 3780 3042 50  0000 L CNN
F 1 "Conn_01x03" H 3780 2951 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3700 3000 50  0001 C CNN
F 3 "~" H 3700 3000 50  0001 C CNN
	1    3700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 900  3900 900 
Wire Wire Line
	4200 1000 3900 1000
Text Label 3900 900  0    50   ~ 0
HV
Text Label 3900 1000 0    50   ~ 0
HV_RET
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J2
U 1 1 600D56DA
P 3650 1900
F 0 "J2" H 3700 2117 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3700 2026 50  0000 C CNN
F 2 "Library:molex" H 3650 1900 50  0001 C CNN
F 3 "~" H 3650 1900 50  0001 C CNN
	1    3650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1900 3150 1900
Wire Wire Line
	3450 2000 3150 2000
Text Label 3150 1900 0    50   ~ 0
VIN
Text Label 3150 2000 0    50   ~ 0
VIN
Wire Wire Line
	3950 2000 4250 2000
Wire Wire Line
	3950 1900 4250 1900
Text Label 4250 2000 2    50   ~ 0
GND
Text Label 4250 1900 2    50   ~ 0
GND
Wire Wire Line
	3300 2350 3000 2350
Wire Wire Line
	3300 2450 3000 2450
Text Label 3000 2350 0    50   ~ 0
GND
Text Label 3000 2450 0    50   ~ 0
GND_S
Wire Wire Line
	3300 2550 3000 2550
Wire Wire Line
	3300 2650 3000 2650
Text Label 3000 2550 0    50   ~ 0
VIN
Text Label 3000 2650 0    50   ~ 0
VIN_S
Wire Wire Line
	3500 2900 3150 2900
Wire Wire Line
	3150 3100 3500 3100
Wire Wire Line
	3450 3500 3150 3500
Wire Wire Line
	3950 3500 4250 3500
Text Label 3150 3500 0    50   ~ 0
LP_Enable
Text Label 4250 3500 2    50   ~ 0
LP_Enable_M
Text Label 3150 2900 0    50   ~ 0
NTC
$Comp
L Device:R R1
U 1 1 600D9B0F
P 3000 3100
F 0 "R1" V 2793 3100 50  0000 C CNN
F 1 "10k" V 2884 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 2930 3100 50  0001 C CNN
F 3 "~" H 3000 3100 50  0001 C CNN
	1    3000 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 3100 2850 3000
Wire Wire Line
	2850 3000 3500 3000
Text Label 3150 3000 0    50   ~ 0
NTC_RET
$Comp
L Connector_Generic:Conn_02x01 J4
U 1 1 600DA6D2
P 3650 3850
F 0 "J4" H 3700 4067 50  0000 C CNN
F 1 "Conn_02x01" H 3700 3976 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3650 3850 50  0001 C CNN
F 3 "~" H 3650 3850 50  0001 C CNN
	1    3650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3850 3150 3850
Wire Wire Line
	3950 3850 4250 3850
Text Label 3150 3850 0    50   ~ 0
HV_RET
Text Label 4250 3850 2    50   ~ 0
GND
$Comp
L Device:C C2
U 1 1 600DB769
P 4900 3850
F 0 "C2" V 4648 3850 50  0000 C CNN
F 1 "C" V 4739 3850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 4938 3700 50  0001 C CNN
F 3 "~" H 4900 3850 50  0001 C CNN
	1    4900 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 3850 5350 3850
Text Label 5350 3850 2    50   ~ 0
GND
Wire Wire Line
	4750 3850 4450 3850
Text Label 4450 3850 0    50   ~ 0
HV_RET
$Comp
L Connector_Generic:Conn_02x01 J5
U 1 1 600DCBCC
P 3650 4200
F 0 "J5" H 3700 4417 50  0000 C CNN
F 1 "Conn_02x01" H 3700 4326 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3650 4200 50  0001 C CNN
F 3 "~" H 3650 4200 50  0001 C CNN
	1    3650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4200 3150 4200
Wire Wire Line
	3950 4200 4250 4200
Text Label 3150 4200 0    50   ~ 0
GND
Text Label 4250 4200 2    50   ~ 0
GND
$Comp
L Device:C C1
U 1 1 600DE169
P 3400 1050
F 0 "C1" H 3515 1096 50  0000 L CNN
F 1 "C" H 3515 1005 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210_HandSoldering" H 3438 900 50  0001 C CNN
F 3 "~" H 3400 1050 50  0001 C CNN
	1    3400 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 600DEB3B
P 3750 900
F 0 "R2" H 3820 946 50  0000 L CNN
F 1 "R" H 3820 855 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3680 900 50  0001 C CNN
F 3 "~" H 3750 900 50  0001 C CNN
	1    3750 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	1350 1350 850  1350
Wire Wire Line
	1350 1450 850  1450
Wire Wire Line
	1350 1750 850  1750
Wire Wire Line
	1350 1650 850  1650
Wire Wire Line
	1350 1550 850  1550
Wire Wire Line
	1350 1850 850  1850
Wire Wire Line
	1350 1950 850  1950
Wire Wire Line
	1350 2050 850  2050
Wire Wire Line
	2850 1350 2350 1350
Wire Wire Line
	2850 1650 2350 1650
Wire Wire Line
	2850 1750 2350 1750
Wire Wire Line
	2850 1850 2350 1850
Wire Wire Line
	2850 1950 2350 1950
Wire Wire Line
	2850 2050 2350 2050
Text Label 850  1350 0    50   ~ 0
VIN
Text Label 850  1450 0    50   ~ 0
VIN
Text Label 2850 1350 2    50   ~ 0
VIN
Text Label 2850 1750 2    50   ~ 0
LP_Enable_M
Text Label 2850 1850 2    50   ~ 0
GND
Text Label 2850 1950 2    50   ~ 0
GND
Text Label 2850 2050 2    50   ~ 0
GND
Text Label 850  2050 0    50   ~ 0
GND
Text Label 850  1950 0    50   ~ 0
GND
Text Label 850  1850 0    50   ~ 0
GND
Text Label 850  1550 0    50   ~ 0
NTC
Text Label 850  1650 0    50   ~ 0
NTC_RET
Text Label 850  1750 0    50   ~ 0
HV
$Comp
L 1017505:1017505 J7
U 1 1 600EA42D
P 3300 2350
F 0 "J7" H 3700 2615 50  0000 C CNN
F 1 "1017505" H 3700 2524 50  0000 C CNN
F 2 "Library:1017505" H 3950 2450 50  0001 L CNN
F 3 "https://www.phoenixcontact.com/online/portal/gb/?uri=pxc-oc-itemdetail:pid=1017505&library=gben&pcck=P-11-01-05&tab=1&selectedCategory=ALL" H 3950 2350 50  0001 L CNN
F 4 "Phoenix Contact TDPT Series 5.08mm Pitch, PCB Terminal Block, PCB Mount, 4 Way" H 3950 2250 50  0001 L CNN "Description"
F 5 "19.2" H 3950 2150 50  0001 L CNN "Height"
F 6 "Phoenix Contact" H 3950 2050 50  0001 L CNN "Manufacturer_Name"
F 7 "1017505" H 3950 1950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "651-1017505" H 3950 1850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Phoenix-Contact/1017505?qs=F5EMLAvA7IDSv5uM7A6HhA%3D%3D" H 3950 1750 50  0001 L CNN "Mouser Price/Stock"
F 10 "1017505" H 3950 1650 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/1017505/phoenix-contact" H 3950 1550 50  0001 L CNN "Arrow Price/Stock"
	1    3300 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2350 4400 2350
Wire Wire Line
	4100 2450 4400 2450
Text Label 4400 2350 2    50   ~ 0
GND
Text Label 4400 2450 2    50   ~ 0
GND_S
Wire Wire Line
	4100 2550 4400 2550
Wire Wire Line
	4100 2650 4400 2650
Text Label 4400 2550 2    50   ~ 0
VIN
Text Label 4400 2650 2    50   ~ 0
VIN_S
Text Label 2850 1650 2    50   ~ 0
VIN
Text Label 2850 1550 2    50   ~ 0
VIN
Text Label 2850 1450 2    50   ~ 0
VIN
Wire Wire Line
	2850 1550 2350 1550
Wire Wire Line
	2850 1450 2350 1450
Wire Wire Line
	1600 2550 2400 2550
Wire Wire Line
	1600 2800 2400 2800
Text Label 1600 2550 0    50   ~ 0
GND_S
Text Label 2400 2550 2    50   ~ 0
GND
Text Label 1600 2800 0    50   ~ 0
VIN_S
Text Label 2400 2800 2    50   ~ 0
VIN
Wire Wire Line
	3400 900  3600 900 
Wire Wire Line
	3400 1200 3900 1200
Wire Wire Line
	3900 1200 3900 1000
$EndSCHEMATC
